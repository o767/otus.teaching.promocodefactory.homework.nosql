﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _mongoDatabase;

        public MongoInitializer(IMongoDatabase mongoDatabase)
        {
            _mongoDatabase = mongoDatabase;
        }
        
        public void InitializeDb()
        {
            const string collectionEmployeesName = nameof(Employee) + "s";
            const string collectionRolesName = nameof(Role) + "s";

            _mongoDatabase.DropCollection(collectionEmployeesName);
            _mongoDatabase.DropCollection(collectionRolesName);

            var employeeCollection = _mongoDatabase.GetCollection<Employee>(collectionEmployeesName);
            var roleCollection = _mongoDatabase.GetCollection<Role>(collectionRolesName);

            employeeCollection.InsertMany(FakeDataFactory.Employees);
        }
    }
}